'use strict';

window.paginator = (function(){

  var page = 1
  ,   loadCount = 3 // The number of items to display/load into feed
  ,   template = Handlebars.compile($('#item').html())
  ,   feed = [];

  function updateFeed(data){ 
    if(typeof data === 'object') {
      if(data.posts) {
        feed.push.apply(feed, data.posts);
      }
    }
    
    displayItems();

  }

  function displayItems(){
    if(feed.length <= loadCount) {
      loadMore();
    } else {
      $('#metro-feed').append(template(feed.splice(0,loadCount)));
    }

  }

  function loadMore(){
    $.ajax({
      url: 'https://api.metro.co.uk/news-feed/',
      data: {
        path: 'home', 
        page: page++
      },
      dataType: 'jsonp',
      jsonp : 'callback',
      jsonpCallback: 'paginator.callback'
    });
  };

  loadMore();

  return {
    callback: function(data){
      updateFeed(data);
    },
    more: function(){
      displayItems();
    }
  
  };

})();
